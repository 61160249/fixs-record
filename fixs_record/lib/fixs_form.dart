import 'package:fixs_record/fixs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fixs_record/fixs_service.dart';

const barcolor = Color(0xFFE57373);
const bgcolor = Color(0xFF455A64);
const fieldcolor = Color(0xFF212121);

class FixsForm extends StatefulWidget {
  Fixs fix;
  FixsForm({Key? key, required this.fix}) : super(key: key);

  @override
  _FixsFormState createState() => _FixsFormState(fix);
}

class _FixsFormState extends State<FixsForm> {
  final _formkey = GlobalKey<FormState>();
  Fixs fix;
  _FixsFormState(this.fix);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      appBar: AppBar(
        title: Text('MANAGE'),
        backgroundColor: barcolor,
      ),
      body: Form(
        key: _formkey,
        child: Container(
          padding: EdgeInsets.all(15),
          child: ListView(
            children: [
              TextFormField(
                initialValue: fix.name,
                style: TextStyle(
                    fontSize: 18.0, height: 1.0, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  icon: new Icon(Icons.person, color: Colors.white),
                  labelText: 'ชื่อผู้ฝาก',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.name = value!;
                  });
                },
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 5) {
                    return 'กรุณาใส่ชื่อ';
                  }
                  return null;
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 1.0, color: Colors.greenAccent),
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < 10) {
                    return 'กรุณาใส่เบอร์โทรศัพท์';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.phone,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  icon: new Icon(Icons.phone, color: Colors.white),
                  labelText: 'เบอร์โทรศัพท์',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                onChanged: (String? value) {
                  setState(() {
                    fix.phone = value!;
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 1.0, color: Colors.greenAccent),
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < -1) {
                    return 'กดหนดสถานะ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.status,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  icon: new Icon(Icons.article_outlined, color: Colors.white),
                  labelText: 'สถานะ',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.status = value!;
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                validator: (value) {
                  if (value == null || value.isEmpty || value.length < -1) {
                    return 'กดหนดราคา';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.price.toString(),
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  icon:
                      new Icon(Icons.attach_money_rounded, color: Colors.white),
                  labelText: 'ค่าบริการ',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                onChanged: (String? value) {
                  setState(() {
                    fix.price = int.parse(value!);
                  });
                },
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 10, 10),
                child: Text(
                  'รายการที่ 1',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.model,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'ชื่อ / รุ่น',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.model = value!;
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.amount.toString(),
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'จำนวน',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                onChanged: (String? value) {
                  setState(() {
                    fix.amount = int.parse(value!);
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.detail,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'อาการ',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.detail = value!;
                  });
                },
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 10, 10),
                child: Text(
                  'รายการที่ 2',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.model1,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'ชื่อ / รุ่น',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.model1 = value!;
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.amount1.toString(),
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'จำนวน',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                onChanged: (String? value) {
                  setState(() {
                    fix.amount1 = int.parse(value!);
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.detail1,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'อาการ',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.detail1 = value!;
                  });
                },
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 10, 10),
                child: Text(
                  'รายการที่ 3',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.model2,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'ชื่อ / รุ่น',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.model2 = value!;
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.amount2.toString(),
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'จำนวน',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                onChanged: (String? value) {
                  setState(() {
                    fix.amount2 = int.parse(value!);
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.detail2,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'อาการ',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.detail2 = value!;
                  });
                },
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 10, 10),
                child: Text(
                  'รายการที่ 4',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.model3,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'ชื่อ / รุ่น',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.model3 = value!;
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.amount3.toString(),
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'จำนวน',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                onChanged: (String? value) {
                  setState(() {
                    fix.amount3 = int.parse(value!);
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.detail3,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'อาการ',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.detail3 = value!;
                  });
                },
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 10, 10),
                child: Text(
                  'รายการที่ 5',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                ),
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.model4,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'ชื่อ / รุ่น',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.model4 = value!;
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.amount4.toString(),
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'จำนวน',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                onChanged: (String? value) {
                  setState(() {
                    fix.amount4 = int.parse(value!);
                  });
                },
              ),
              TextFormField(
                style: TextStyle(
                    fontSize: 18.0, height: 0.8, color: Colors.greenAccent),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                initialValue: fix.detail4,
                decoration: InputDecoration(
                  fillColor: fieldcolor,
                  labelText: 'อาการ',
                  labelStyle: TextStyle(color: Colors.white, fontSize: 18.0),
                  filled: true,
                ),
                onChanged: (String? value) {
                  setState(() {
                    fix.detail4 = value!;
                  });
                },
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.save_rounded, color: Colors.white),
        onPressed: () async {
          if (_formkey.currentState!.validate()) {
            if (fix.id > 0) {
              await updateFix(fix);
            } else {
              await addNew(fix);
            }
            Navigator.pop(context);
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
