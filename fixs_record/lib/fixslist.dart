class fixslist {
  final int id;
  int fixsid;
  String name;
  int amount;
  String detail;

  fixslist({
    required this.id,
    required this.name,
    required this.fixsid,
    required this.amount,
    required this.detail,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'amount': amount,
      'detail': detail,
    };
  }

  static List<fixslist> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return fixslist(
        id: maps[i]['id'],
        name: maps[i]['name'],
        fixsid: maps[i]['fixsid'],
        amount: maps[i]['amount'],
        detail: maps[i]['detail'],
      );
    });
  }

  @override
  String toString() {
    return 'fixslist{id: $id, name: $name, fixsid: $fixsid, amount: $amount, detail: $detail';
  }
}
