import 'package:intl/intl.dart';
import 'fixs.dart';

var mockData = [
  Fixs(
    id: 1,
    date: '2021-10-15',
    name: 'คุณสนธิ',
    phone: '0853687951',
    status: 'รอคิว',
    price: 0,
    model: 'ลำโพง 15 นิ้ว',
    amount: 4,
    detail: 'หน้าผ้าขาด',
    model1: 'ลำโพง 18 นิ้ว',
    amount1: 2,
    detail1: 'ไม่ดัง',
    model2: 'power 2500w',
    amount2: 1,
    detail2: 'ไม่ดัง 1 ข้าง',
    model3: 'ไมค์ลอย pro uro',
    amount3: 1,
    detail3: 'เสียงไม่ออก',
    model4: 'ซีดี',
    amount4: 1,
    detail4: 'ไม่เล่นแผ่น',
  ),
  Fixs(
    id: 2,
    date: '2021-10-28',
    name: 'พี่สมพล',
    phone: '0966597135',
    status: 'ซ่อมเสร็จแล้ว',
    price: 500,
    model: 'power mixer YAMAHAAA',
    amount: 1,
    detail: 'เช็ค',
    model1: '',
    amount1: 0,
    detail1: '',
    model2: '',
    amount2: 0,
    detail2: '',
    model3: '',
    amount3: 0,
    detail3: '',
    model4: '',
    amount4: 0,
    detail4: '',
  ),
  Fixs(
    id: 3,
    date: '2021-10-29',
    name: 'คุณอาคาอินุ',
    phone: '0966597135',
    status: 'ซ่อมเสร็จแล้ว',
    price: 1800,
    model: 'ลำโพง 18 นิ้ว JBL',
    amount: 2,
    detail: 'ขาด',
    model1: '',
    amount1: 0,
    detail1: '',
    model2: '',
    amount2: 0,
    detail2: '',
    model3: '',
    amount3: 0,
    detail3: '',
    model4: '',
    amount4: 0,
    detail4: '',
  ),
];
var lastId = 3;
String getDate() {
  DateTime now = DateTime.now();
  String formattedDate = DateFormat('yyyy-MM-dd').format(now);
  return formattedDate;
}

int getNewId() {
  return lastId++;
}

Future<void> updateFix(Fixs fix) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockData.indexWhere((item) => item.id == fix.id);
    mockData[index] = fix;
  });
}

Future<void> addNew(Fixs fix) {
  return Future.delayed(Duration(seconds: 1), () {
    mockData.add(Fixs(
      date: getDate(),
      id: fix.id,
      name: fix.name,
      phone: fix.phone,
      status: fix.status,
      price: fix.price,
      model: fix.model,
      amount: fix.amount,
      detail: fix.detail,
      model1: fix.model1,
      amount1: fix.amount1,
      detail1: fix.detail1,
      model2: fix.model2,
      amount2: fix.amount2,
      detail2: fix.detail2,
      model3: fix.model3,
      amount3: fix.amount3,
      detail3: fix.detail3,
      model4: fix.model4,
      amount4: fix.amount4,
      detail4: fix.detail4,
    ));
  });
}

Future<void> delFixs(Fixs fix) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockData.indexWhere((element) => element.id == fix.id);
    mockData.removeAt(index);
  });
}

Future<List<Fixs>> getFixs() {
  return Future.delayed(Duration(seconds: 1), () => mockData);
}
