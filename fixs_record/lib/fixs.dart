class Fixs {
  final int id;
  String date;
  String name;
  String phone;
  String status;
  int price;
  String model;
  int amount;
  String detail;
  String model1;
  int amount1;
  String detail1;
  String model2;
  int amount2;
  String detail2;
  String model3;
  int amount3;
  String detail3;
  String model4;
  int amount4;
  String detail4;

  Fixs({
    required this.id,
    required this.date,
    required this.name,
    required this.phone,
    required this.status,
    required this.price,
    required this.model,
    required this.amount,
    required this.detail,
    required this.model1,
    required this.amount1,
    required this.detail1,
    required this.model2,
    required this.amount2,
    required this.detail2,
    required this.model3,
    required this.amount3,
    required this.detail3,
    required this.model4,
    required this.amount4,
    required this.detail4,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'date': date,
      'name': name,
      'phone': phone,
      'status': status,
      'price': price,
      'model': model,
      'amount': amount,
      'detail': detail,
      'model1': model1,
      'amount1': amount1,
      'detail1': detail1,
      'model2': model2,
      'amount2': amount2,
      'detail2': detail2,
      'model3': model3,
      'amount3': amount3,
      'detail3': detail3,
      'model4': model4,
      'amount4': amount4,
      'detail4': detail4,
    };
  }

  static List<Fixs> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Fixs(
        id: maps[i]['id'],
        date: maps[i]['date'],
        name: maps[i]['name'],
        phone: maps[i]['phone'],
        status: maps[i]['status'],
        price: maps[i]['price'],
        model: maps[i]['model'],
        amount: maps[i]['amount'],
        detail: maps[i]['detail'],
        model1: maps[i]['model1'],
        amount1: maps[i]['amount1'],
        detail1: maps[i]['detail1'],
        model2: maps[i]['model2'],
        amount2: maps[i]['amount2'],
        detail2: maps[i]['detail2'],
        model3: maps[i]['model3'],
        amount3: maps[i]['amount3'],
        detail3: maps[i]['detail3'],
        model4: maps[i]['model4'],
        amount4: maps[i]['amount4'],
        detail4: maps[i]['detail4'],
      );
    });
  }

  @override
  String toString() {
    return 'Fixs{id: $id, date: $date, name: $name, phone: $phone , status: $status}, price: $price , model: $model, amount: $amount, detail: $detail, model1: $model1, amount1: $amount1, detail1: $detail1,model2: $model2, amount2: $amount2, detail2: $detail2,model3: $model3, amount3: $amount3, detail3: $detail3,model4: $model4, amount4: $amount4, detail4: $detail4';
  }
}
