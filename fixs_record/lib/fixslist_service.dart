import 'package:intl/intl.dart';
import 'fixslist.dart';

var mockData = [
  fixslist(
    id: 1,
    name: 'แอมป์ AWP',
    fixsid: 1,
    amount: 1,
    detail: "เสียงไม่ออก 1 ข้าง",
  ),
  fixslist(
      id: 2,
      name: 'มิกเซอร์ AK47',
      fixsid: 2,
      amount: 1,
      detail: "เช็คทั้งเครื่อง"),
  fixslist(
      id: 3,
      name: 'ลำโพง 18 นิ้ว',
      fixsid: 2,
      amount: 2,
      detail: "เปลี่ยนว้อย"),
];
var lastId = 3;

int getNewId() {
  return lastId++;
}

Future<void> addNew(fixslist fix) {
  return Future.delayed(Duration(seconds: 1), () {
    mockData.add(fixslist(
      id: fix.id,
      name: fix.name,
      fixsid: fix.fixsid,
      amount: fix.amount,
      detail: fix.detail,
    ));
  });
}

Future<void> delfixslist(fixslist fixslist) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockData.indexWhere((element) => element.id == fixslist.id);
    mockData.removeAt(index);
  });
}

Future<List<fixslist>> getfixslists() {
  return Future.delayed(Duration(seconds: 1), () => mockData);
}
