import 'package:fixs_record/fixs.dart';
import 'package:fixs_record/fixs_service.dart';
import 'package:fixs_record/fixs_form.dart';
import 'package:flutter/material.dart';

const barcolor = Color(0xFFE57373);
const bgcolor = Color(0xFF455A64);

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late Future<List<Fixs>> _fixs;
  @override
  void initState() {
    super.initState();
    _fixs = getFixs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      appBar: AppBar(
        title: Text('HOME'),
        backgroundColor: barcolor,
        actions: [
          IconButton(
            padding: EdgeInsets.fromLTRB(0, 0, 70, 0),
            onPressed: () async {
              Fixs newfix = Fixs(
                id: -1,
                date: '',
                name: '',
                phone: '',
                status: '',
                price: 0,
                model: '',
                amount: 0,
                detail: '',
                model1: '',
                amount1: 0,
                detail1: '',
                model2: '',
                amount2: 0,
                detail2: '',
                model3: '',
                amount3: 0,
                detail3: '',
                model4: '',
                amount4: 0,
                detail4: '',
              );
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FixsForm(fix: newfix)));
              setState(() {
                _fixs = getFixs();
              });
              // await _loadProfile();
            },
            icon: Icon(Icons.add_box_rounded, size: 50),
          ),
        ],
      ),
      body: FutureBuilder(
        future: _fixs,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          List<Fixs> fixs = snapshot.data as List<Fixs>;
          return ListView.builder(
            itemBuilder: (context, index) {
              var fix = fixs.elementAt(index);
              return ListTile(
                title: Card(
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(
                          Icons.folder_open_rounded,
                          color: Colors.black,
                          size: 50,
                        ),
                        title: Text(
                          '',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                          ),
                        ),
                        subtitle: Text(
                          'ชื่อ : ${fix.name}\nวันที่ : ${fix.date}\nเบอโทรศัพท์ : ${fix.phone}\nสถานะ : ${fix.status}\nค่าบริการ : ${fix.price} บาท',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                          ),
                        ),
                        onTap: () async {
                          await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FixsForm(fix: fix)));
                          setState(() {
                            _fixs = getFixs();
                          });
                        },
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            icon: const Icon(Icons.create_rounded,
                                color: Colors.black, size: 25),
                            tooltip: 'EDIT',
                            onPressed: () async {
                              await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          FixsForm(fix: fix)));
                              setState(() {
                                _fixs = getFixs();
                              });
                            },
                          ),
                          IconButton(
                            icon: const Icon(Icons.delete_rounded,
                                color: Colors.black, size: 25),
                            tooltip: 'DELETE',
                            onPressed: () async {
                              await delFixs(fix);
                              setState(() {
                                _fixs = getFixs();
                              });
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            },
            itemCount: fixs.length,
          );
        },
      ),
    );
  }
}
